package com.tomatheb.raxpackTweak;



import com.tomatheb.raxpackTweak.handlers.ConfigurationHandler;
import com.tomatheb.raxpackTweak.handlers.InteractHandler;
import com.tomatheb.raxpackTweak.proxy.IProxy;
import com.tomatheb.raxpackTweak.reference.Reference;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MOD_ID, name=Reference.MOD_NAME, version = Reference.VERSION,
        guiFactory = Reference.ROOT + "handlers.ConfigGUIHandler", acceptedMinecraftVersions = Reference.MC_VERSION)
public class RaxpackTweak
{
    @Mod.Instance(Reference.MOD_ID)
    public static RaxpackTweak instance;

    @SidedProxy(clientSide=Reference.ROOT+"proxy.ClientProxy", serverSide=Reference.ROOT+"proxy.ServerProxy")
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        ConfigurationHandler.init(event.getSuggestedConfigurationFile());
        MinecraftForge.EVENT_BUS.register(new ConfigurationHandler());

        proxy.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(instance);

        proxy.registerEventHandlers();
        proxy.init();
    }

    @Mod.EventHandler
    public void postInit(FMLInitializationEvent event)
    {

    }
}
