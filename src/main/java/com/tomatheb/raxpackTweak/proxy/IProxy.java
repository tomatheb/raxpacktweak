package com.tomatheb.raxpackTweak.proxy;

public interface IProxy
{
    public abstract void registerEventHandlers();
    public abstract void init();
    public abstract void preInit();
    public abstract void updateEffectiveConfigValues();
}
