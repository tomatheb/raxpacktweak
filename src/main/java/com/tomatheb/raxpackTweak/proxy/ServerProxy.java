package com.tomatheb.raxpackTweak.proxy;

import com.tomatheb.raxpackTweak.handlers.ServerConnectHandler;
import com.tomatheb.raxpackTweak.reference.Configurations;
import net.minecraftforge.common.MinecraftForge;

public class ServerProxy extends CommonProxy
{
    @Override
    public void registerEventHandlers() {
        super.registerEventHandlers();

        ServerConnectHandler serverConnectHandler = new ServerConnectHandler();
        MinecraftForge.EVENT_BUS.register(serverConnectHandler);
    }


}
