package com.tomatheb.raxpackTweak.proxy;

import com.tomatheb.raxpackTweak.handlers.InteractHandler;
import com.tomatheb.raxpackTweak.handlers.WorldUnloadHandler;
import com.tomatheb.raxpackTweak.packets.ConfigSync;
import com.tomatheb.raxpackTweak.packets.ConfigSyncHandler;
import com.tomatheb.raxpackTweak.packets.RaxpackTweakPacketHandler;
import com.tomatheb.raxpackTweak.reference.Configurations;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;

public class CommonProxy implements IProxy{

    @Override
    public void registerEventHandlers() {
        InteractHandler interactHandler = new InteractHandler();
        WorldUnloadHandler worldUnloadHandler = new WorldUnloadHandler();

        MinecraftForge.EVENT_BUS.register(interactHandler);
        MinecraftForge.EVENT_BUS.register(worldUnloadHandler);
    }

    @Override
    public void init() {
        RaxpackTweakPacketHandler.INSTANCE.registerMessage(ConfigSyncHandler.class, ConfigSync.class, 0, Side.CLIENT);
    }

    @Override
    public void preInit() {
        //NOOP
    }

    @Override
    public void updateEffectiveConfigValues() {
        Configurations.CanteenTweak.server.cleanliness=Configurations.CanteenTweak.cleanliness;
        Configurations.CanteenTweak.server.enabled=Configurations.CanteenTweak.enabled;
        Configurations.CanteenTweak.server.bottle=Configurations.CanteenTweak.bottle;
    }


}
