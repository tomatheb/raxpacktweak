package com.tomatheb.raxpackTweak.handlers;

import com.tomatheb.raxpackTweak.helper.LogHelper;
import com.tomatheb.raxpackTweak.packets.ConfigSync;
import com.tomatheb.raxpackTweak.packets.RaxpackTweakPacketHandler;
import com.tomatheb.raxpackTweak.reference.Configurations;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class ServerConnectHandler
{
    @SubscribeEvent
    public void onLogIn(PlayerEvent.PlayerLoggedInEvent event)
    {
        LogHelper.info("Config Packet Sent");
        RaxpackTweakPacketHandler.INSTANCE.sendTo(new ConfigSync(Configurations.CanteenTweak.cleanliness, Configurations.CanteenTweak.enabled, Configurations.CanteenTweak.bottle), (EntityPlayerMP) event.player);
    }
}
