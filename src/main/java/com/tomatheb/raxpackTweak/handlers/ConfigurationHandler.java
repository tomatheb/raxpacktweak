package com.tomatheb.raxpackTweak.handlers;

import com.tomatheb.raxpackTweak.RaxpackTweak;
import com.tomatheb.raxpackTweak.reference.Configurations;
import com.tomatheb.raxpackTweak.reference.Reference;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.io.File;

public class ConfigurationHandler {
    public static Configuration configuration;

    public static void init(File configFile) {
        if (configuration == null) {
            //create config option from given file
            configuration = new Configuration(configFile);
            loadConfiguration();
        }

    }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (event.getModID().equalsIgnoreCase(Reference.MOD_ID)) {
            // Resync config
            loadConfiguration();
        }
    }

    private static void loadConfiguration() {
        //define configs here
        // eg.
        Configurations.CanteenTweak.enabled = configuration.getBoolean("enabled", Configuration.CATEGORY_GENERAL,
        true, "Set to false to disable canteen tweak");

        Configurations.CanteenTweak.cleanliness = configuration.getInt("cleanliness", Configuration.CATEGORY_GENERAL, 2, 1,3,
                "The water a sink returns; 1:Dirty, 2:Filtered, 3:Clean");

        Configurations.CanteenTweak.bottle = configuration.getBoolean("waterBottle", Configuration.CATEGORY_GENERAL,
                true, "Whether glass bottles should also be filled by sinks");

        RaxpackTweak.proxy.updateEffectiveConfigValues();
        if (configuration.hasChanged()) {
            configuration.save();

        }
    }
}
