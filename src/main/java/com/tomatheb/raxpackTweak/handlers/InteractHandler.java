package com.tomatheb.raxpackTweak.handlers;

import com.tomatheb.raxpackTweak.helper.LogHelper;
import com.tomatheb.raxpackTweak.reference.Configurations;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.PotionTypes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameData;

public class InteractHandler {

    @SubscribeEvent
    public void onPlayerInteract(PlayerInteractEvent.RightClickBlock event) {
        if (Configurations.CanteenTweak.server.enabled) {
            ItemStack itemStack = event.getItemStack();
            Item item = itemStack.getItem();
            //LogHelper.info(item.getRegistryName());
            EntityPlayer player = event.getEntityPlayer();
            RayTraceResult traceResult = rayTrace(player.world, player, false);
            if (traceResult == null || item.getRegistryName() == null) {
                return;
            }
            Block block = player.world.getBlockState(traceResult.getBlockPos()).getBlock();
            String itemMod = item.getRegistryName().getResourceDomain();
            if (block.getUnlocalizedName().equals("tile.cookingforblockheads:sink")) {
                if (item.getUnlocalizedName().equals("item.canteen") && itemMod.equals("toughasnails")) {
                    int meta;

                    meta = Configurations.CanteenTweak.server.cleanliness;

                    itemStack.setItemDamage(meta);
                } else if (item == Items.GLASS_BOTTLE && Configurations.CanteenTweak.server.bottle && Loader.isModLoaded("toughasnails")){
                    ItemStack bottle;
                    switch (Configurations.CanteenTweak.server.cleanliness) {
                        case 1:
                            bottle = new ItemStack(Item.getByNameOrId("toughasnails:water_bottle"), 1, 0);
                            break;
                        case 2:
                            bottle = new ItemStack(Item.getByNameOrId("toughasnails:water_bottle"), 1, 1);
                            break;
                        default:
                            bottle = PotionUtils.addPotionToItemStack(new ItemStack(Items.POTIONITEM), PotionTypes.WATER);
                            break;
                    }
                    int count=itemStack.getCount();
                    LogHelper.info(itemStack.serializeNBT());
                    itemStack.setCount(count-1);
                    if (!player.inventory.addItemStackToInventory(bottle) && !player.world.isRemote){
                        player.dropItem(bottle, false);
                    }
                    event.setCanceled(true);
                }
            }
        }
    }

    private RayTraceResult rayTrace(World worldIn, EntityPlayer playerIn, boolean useLiquids) {
        float f = playerIn.rotationPitch;
        float f1 = playerIn.rotationYaw;
        double d0 = playerIn.posX;
        double d1 = playerIn.posY + (double) playerIn.getEyeHeight();
        double d2 = playerIn.posZ;
        Vec3d vec3d = new Vec3d(d0, d1, d2);
        float f2 = MathHelper.cos(-f1 * 0.017453292F - (float) Math.PI);
        float f3 = MathHelper.sin(-f1 * 0.017453292F - (float) Math.PI);
        float f4 = -MathHelper.cos(-f * 0.017453292F);
        float f5 = MathHelper.sin(-f * 0.017453292F);
        float f6 = f3 * f4;
        float f7 = f2 * f4;
        double d3 = 5.0D;
        if (playerIn instanceof net.minecraft.entity.player.EntityPlayerMP) {
            d3 = ((net.minecraft.entity.player.EntityPlayerMP) playerIn).interactionManager.getBlockReachDistance();
        }
        Vec3d vec3d1 = vec3d.addVector((double) f6 * d3, (double) f5 * d3, (double) f7 * d3);
        return worldIn.rayTraceBlocks(vec3d, vec3d1, useLiquids, !useLiquids, false);
    }

}
