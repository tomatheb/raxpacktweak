package com.tomatheb.raxpackTweak.handlers;

import com.tomatheb.raxpackTweak.RaxpackTweak;
import com.tomatheb.raxpackTweak.helper.LogHelper;
import net.minecraft.client.Minecraft;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class WorldUnloadHandler {
    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onWorldUnload(WorldEvent.Unload event)
    {
        if (event.getWorld().isRemote && !Minecraft.getMinecraft().getConnection().getNetworkManager().isChannelOpen())
        {
            RaxpackTweak.proxy.updateEffectiveConfigValues();
            LogHelper.info("Configuration restored to local values");
        }
    }
}
