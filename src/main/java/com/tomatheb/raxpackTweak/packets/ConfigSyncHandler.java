package com.tomatheb.raxpackTweak.packets;

import com.tomatheb.raxpackTweak.reference.Configurations;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ConfigSyncHandler implements IMessageHandler<ConfigSync, IMessage>
{
    @Override public IMessage onMessage(ConfigSync message, MessageContext ctx) {

        // The value that was sent
        int canteen_clean = message.canteen_clean;
        boolean canteen_enabled = message.canteen_enabled;
        boolean bottle = message.water_bottles;

        Configurations.CanteenTweak.server.cleanliness = canteen_clean;
        Configurations.CanteenTweak.server.enabled = canteen_enabled;
        Configurations.CanteenTweak.server.bottle = bottle;

        // No response packet
        return null;
    }
}
