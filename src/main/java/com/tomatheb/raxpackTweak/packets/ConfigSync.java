package com.tomatheb.raxpackTweak.packets;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class ConfigSync implements IMessage {

    // A default constructor is always required
    public ConfigSync(){}

    int canteen_clean;
    boolean canteen_enabled;
    boolean water_bottles;
    public ConfigSync(int clean, boolean enabled, boolean bottles) {
        this.canteen_clean = clean;
        this.canteen_enabled = enabled;
        this.water_bottles = bottles;
    }


    @Override
    public void fromBytes(ByteBuf buf) {
        canteen_clean=buf.readInt();
        canteen_enabled=buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(canteen_clean);
        buf.writeBoolean(canteen_enabled);
    }
}
