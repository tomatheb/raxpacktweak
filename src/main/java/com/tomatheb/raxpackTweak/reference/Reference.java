package com.tomatheb.raxpackTweak.reference;


public class Reference
{
    public static final String MOD_ID = "raxpacktweak";
    public static final String MOD_NAME = "Raxpack Tweak";
    public static final String VERSION = "0.2";
    public static final String MC_VERSION = "1.11.2";
    public static final String ROOT = "com.tomatheb.raxpackTweak.";
}
