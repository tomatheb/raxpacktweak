package com.tomatheb.raxpackTweak.reference;

public class Configurations
{
    public static class CanteenTweak
    {
        public static boolean enabled;
        public static int cleanliness;
        public static boolean bottle;
        public static class server
        {
            public static boolean enabled;
            public static int cleanliness;
            public static boolean bottle;
        }
    }
}
